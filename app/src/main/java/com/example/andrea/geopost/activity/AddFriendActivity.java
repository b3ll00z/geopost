package com.example.andrea.geopost.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddFriendActivity extends AppCompatActivity {

    private List<String> usernames = new ArrayList<>();
    private ListView usernamesView;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        //Collego adapter con lista di amici
        adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, usernames);
        usernamesView = findViewById(R.id.friend_list);
        usernamesView.setAdapter(adapter);

        usernamesView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String username = (String) parent.getItemAtPosition(position);

                        AlertDialog.Builder builder = new AlertDialog.Builder(AddFriendActivity.this);
                        builder.setTitle("Conferma amicizia")
                                .setIcon(R.drawable.add_friend_icon)
                                .setMessage("Vuoi aggiungere " + username + "?")
                                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        addUser(username);
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .create()
                                .show();
                    }
                }
        );

        SearchView searchView = findViewById(R.id.find_friend);
        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        searchUsers(newText);
                        return false;
                    }
                }
        );
    }

    private void searchUsers(String start) {
        String parametersForURL = "&usernamestart=" + start + "&limit=10";
        VolleyRequestHandler
                .getInstance(getApplicationContext(), null)
                .searchUsers(parametersForURL, new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            JSONArray users = response.getJSONArray("usernames");

                            if (adapter != null) {
                                adapter.clear();
                                usernames.clear();
                            }

                            Log.d("AddFriendActivity", "Ready to fill adapter!");
                            for (int userIndex = 0; userIndex < users.length(); userIndex++)
                                usernames.add(users.getString(userIndex));

                            adapter.notifyDataSetChanged();
                            Log.d("AddFriendActivity", "Adapter filled!");

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "Caricamento adapter fallito", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getApplicationContext(), "Errore caricamento username", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        return;
                    }
                });
    }


    private void addUser(String username) {
        String parametersForURL = "&username=" + username;
        VolleyRequestHandler
                .getInstance(getApplicationContext(), null)
                .addUser(parametersForURL, new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        return;
                    }

                    @Override
                    public void onError(String error) {
                        String errorMessage = null;
                        if (error.equals("CANNOT FOLLOW YOURSELF"))
                            errorMessage = "Non puoi seguire te stesso!";
                        if (error.equals("ALREADY FOLLOWING USER"))
                            errorMessage = "Già segui questo utente!";
                        if (error.equals("USERNAME NOT FOUND"))
                            errorMessage = "Utente non trovato!";

                        Toast.makeText(AddFriendActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        Log.d("AddFriendActivity", "Errore 400: " + errorMessage);
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        Toast.makeText(AddFriendActivity.this, response, Toast.LENGTH_LONG).show();
                        comeBackToHome();
                    }
                });
    }

    private void comeBackToHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        comeBackToHome();
    }
}
