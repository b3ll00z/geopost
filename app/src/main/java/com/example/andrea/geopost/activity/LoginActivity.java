package com.example.andrea.geopost.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Activation of registration url link
        TextView linkToRegistrationView = (TextView) findViewById(R.id.link_registration);
        linkToRegistrationView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * Metodo su onClick del bottone di login
     *
     * @param view bottone di login
     */
    public void loginToGeoPost(View view) {
        EditText usernameView = (EditText) findViewById(R.id.username);
        String username = usernameView.getText().toString();
        Log.d("LoginActivity", "Username read: " + username);

        EditText passwordView = (EditText) findViewById(R.id.password);
        String password = passwordView.getText().toString();
        Log.d("LoginActivity", "Password read: " + password);

        login(username, password);
    }


    private void login(String username, String psw) {
        VolleyRequestHandler
                .getInstance(getApplicationContext(), null)
                .login(username, psw, new DataFetchCallBack() {

                    public void onSuccess(JSONObject response) {
                        return;
                    }

                    @Override
                    public void onError(String error) {
                        Log.d("LoginActivity", "That didn't work! ");
                        TextView errorTextView = (TextView) findViewById(R.id.loginError);
                        errorTextView.setText("Username or Password non corretti!");
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        showProgressDialog();
                        Log.d("GeoPostRequests", "Response is: " + response);
                        startHomeActivity(response, username);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        TextView errorTextView = (TextView) findViewById(R.id.loginError);
        errorTextView.setVisibility(View.INVISIBLE);
    }

    /* Prima di passare a HomeActivity immetto nelle SharedPreferences session_id
     *  e username, in modo che siano utilizzabili dalle altre activity
     */
    private void startHomeActivity(String session_id, String username) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("SESSION_ID", session_id);
        editor.putString("USERNAME", username);
        editor.commit();

        startActivity(new Intent(this, MainActivity.class));
    }

    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Caricamento...");
        progressDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
