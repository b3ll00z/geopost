package com.example.andrea.geopost.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.fragments.FriendsFragment;
import com.example.andrea.geopost.fragments.FriendsMapFragment;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_ACCESS_FINE_LOCATION_PERMISSION = 1;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private String session_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Controllo i permessi per la geolocalizzazione
        checkGeoPostLocationPermission();

        //Memorizzo il session ID nelle shared preferences
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        session_id = sharedPref.getString("SESSION_ID", null);
        Log.d("MainActivity", "session_id is " + session_id);

        //Setto la UI
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i = new Intent();
        Class classToGo = null;
        switch (id) {
            case R.id.add_friend:
                classToGo = AddFriendActivity.class;
                break;
            case R.id.update_status:
                classToGo = UpdateStatusActivity.class;
                break;
            case R.id.personal_profile:
                classToGo = PersonalProfileActivity.class;
                break;
        }
        i.setClass(getApplicationContext(), classToGo);
        startActivity(i);
        return super.onOptionsItemSelected(item);
    }

    private void checkGeoPostLocationPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                Log.d("MainActivity", "Questo permesso consente ai tuoi amici di sapere la tua posizione");

                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage("Attiva la geolocalizzazione, i tuoi amici sapranno dove sei!")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestAccessFineLocationPermission();
                            }
                        })
                        .create()
                        .show();
            } else
                requestAccessFineLocationPermission();
        }
    }

    private void requestAccessFineLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION_PERMISSION);
        Log.d("MainActivity", "Richiesta permesso!");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Perfetto, mostra ai tuoi amici dove sei! :)", Toast.LENGTH_SHORT).show();

                    Log.d("MainActivity", "Perfetto, mostra ai tuoi amici dove sei! :)");
                } else
                    startActivity(new Intent(this, WarningPermissionActivity.class));
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FriendsFragment();
                case 1:
                    return new FriendsMapFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.friends_list);
                case 1:
                    return getString(R.string.map_friends);
            }
            return null;
        }
    }
}