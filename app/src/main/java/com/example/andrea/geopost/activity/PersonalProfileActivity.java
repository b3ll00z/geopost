package com.example.andrea.geopost.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

public class PersonalProfileActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap googleMap = null;
    private String sessionID;
    private String username;
    private String msg;
    private double lat;
    private double lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_profile);

        //Rilevazione username
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sessionID = sharedPref.getString("SESSION_ID", null);
        Log.d("PersonalProfileActivity", "SESSION_ID: " + sessionID);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button logoutButton = findViewById(R.id.logoutButton);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutRequest();
            }
        });
    }

    /* Il caricamento della mappa è asincrono, da tenere as-is */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        Log.d("PersonalProfileActivity", "Mappa caricata");
        fetchProfile();
    }

    private void fetchProfile() {

        VolleyRequestHandler.getInstance(getApplicationContext(), null)
                .profile(new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        try {
                            username = response.getString("username");
                            msg = response.getString("msg");
                            lat = response.getDouble("lat");
                            lon = response.getDouble("lon");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            setProfileLayout();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getApplicationContext(), "Errore rilevamento profilo", Toast.LENGTH_SHORT).show();
                        Log.d("PersonalProfileActivity", "Errore rilevamento profilo");
                    }

                    @Override
                    public void onStringRequestSuccess(String response) { return; }
                });
    }

    private void setProfileLayout() {

        //setto il campo di testo corrispondente al nome utentee
        TextView usernameTextView = findViewById(R.id.username);
        usernameTextView.setText(username);

        //setto il campo di testo corrispondente al nome utente
        if(!msg.equals("null")){
            TextView msgTextView = findViewById(R.id.lastMessage);
            msgTextView.setText(msg);
        }

        if(lat != 0.0 && lon != 0.0)
            setUpMap();
    }

    /* Posiziono il marker dell'ultima posizione registrata */
    private void setUpMap() {

        LatLng position = new LatLng(lat, lon);

        Log.d("PersonalProfileActivity", "New latitude acquired: " + lat);
        Log.d("PersonalProfileActivity", "New longitude acquired: " + lon);

        googleMap.addMarker(new MarkerOptions()
                .position(position)
                .title(username)
                .snippet(msg)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        Log.d("PersonalProfileActivity", "Aggiunto marker sulla mappa");

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 1));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        Log.d("PersonalProfileActivity", "Camera zoomata");
    }

    private void logoutRequest(){
        VolleyRequestHandler
                .getInstance(getApplicationContext(), sessionID)
                .logout(new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {return;}

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getApplicationContext(), "Errore nel logout", Toast.LENGTH_SHORT).show();
                        Log.d("PersonalProfileActivity", "Errore nel logout");
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        //Elimino attuale session_id e torno a LoginActivity
                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("SESSION_ID", null);
                        editor.commit();
                        Log.d("PersonalProfileActivity", "session_id destroyed");
                        returnToLoginActivity();
                    }
                });
    }

    private void returnToLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
