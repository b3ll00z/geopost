package com.example.andrea.geopost.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import org.json.JSONObject;

import static com.example.andrea.geopost.activity.MainActivity.REQUEST_ACCESS_FINE_LOCATION_PERMISSION;

public class UpdateStatusActivity extends AppCompatActivity implements View.OnClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Button updateStatusBtn;
    private LocationManager locationManager;
    private GoogleApiClient mGoogleApiClient = null;
    private String newStatus;
    private Location lastKnownLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_status);

        //Rilevazione session_id
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String session_id = sharedPref.getString("SESSION_ID", null);
        Log.d("UpdateStatusActivity", "Session_id is " + session_id);

        // Instantiate and connect GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();

        updateStatusBtn = findViewById(R.id.updateStatusBtn);
        updateStatusBtn.setOnClickListener(this);
    }

    public void onClick(View view) {

            //Acquisizione tweet
            EditText newStatusView = findViewById(R.id.statusEditText);
            newStatus = newStatusView.getText().toString();

            //Calcolo posizione
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            //Apertura Dialog per attivare geolocalizzazione, se non è settata
            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Geolocalizzazione disattivata")
                        .setIcon(R.drawable.turned_off_geolocalization_icon)
                        .setMessage("Per favore, attiva la geolocalizzazione per continuare")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .create()
                        .show();
            } else {
                checkAndActivatePermission();
                getLocation();
            }
    }

    private void checkAndActivatePermission() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION_PERMISSION);
    }

    private void getLocation() {

        //Serve per ottenere la posizione, qui si configurano ogni quanto aggiornarla, ecc.
        LocationRequest mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(10000);
        //mLocationRequest.setFastestInterval(5000);


        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        checkAndActivatePermission();
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("UpdateStatusActivity", "Google API services connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("UpdateStatusActivity", "Google API connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("UpdateStatusActivity", "Google API connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {

        double newLat = location.getLatitude();
        double newLon = location.getLongitude();

        if (lastKnownLocation == null || !(newLat == lastKnownLocation.getLatitude() && newLon == lastKnownLocation.getLongitude())){
            lastKnownLocation = location;
            Log.d("UpdateStatusActivity", "loation changed");
            updateNewStatus(location);
        }
    }

    private void updateNewStatus(Location location){

        double newLatitude  = location.getLatitude();
        double newLongitude = location.getLongitude();
        String parametersForURL= "&message=" + newStatus + "&lat=" + newLatitude  + "&lon=" + newLongitude;

        VolleyRequestHandler
                .getInstance(getApplicationContext(), null)
                .updateStatus(parametersForURL, new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {return;}

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getApplicationContext(), "Errore aggiornamento stato", Toast.LENGTH_SHORT).show();
                        Log.d("UpdateStatusActivity", "Errore aggiornamento stato");
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        startProfileActivity(newStatus, location);
                    }
                });
    }

    private void startProfileActivity(String newStatus, Location newLocation) {

        Toast.makeText(getApplicationContext(), "Stato aggiornato!", Toast.LENGTH_SHORT).show();

        Log.d("UpdateStatusActivity", "Status: "   + newStatus);
        Log.d("UpdateStatusActivity", "Location: " + newLocation.toString());

        Intent intent = new Intent(this, PersonalProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

}
