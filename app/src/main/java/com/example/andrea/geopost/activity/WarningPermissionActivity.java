package com.example.andrea.geopost.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.andrea.geopost.R;

public class WarningPermissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning_permission);

        Button warning_button = findViewById(R.id.warning_button);
        warning_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestLocationPermission();
                    }
                }
        );
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.REQUEST_ACCESS_FINE_LOCATION_PERMISSION);
        Log.d("WarningPermActivity", "Richiesta permesso!");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){

        switch (requestCode) {
            case MainActivity.REQUEST_ACCESS_FINE_LOCATION_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Perfetto, mostra ai tuoi amici dove sei! :)", Toast.LENGTH_SHORT).show();
                    Log.d("WarningPermActivity", "Perfetto, mostra ai tuoi amici dove sei! :)");
                    startActivity(new Intent(this, MainActivity.class));
                }
                else {
                    Toast.makeText(this, "Permesso rifiutato :(", Toast.LENGTH_SHORT).show();
                    Log.d("WarningPermActivity", "Permesso rifiutato :(");
                }
        }
    }
}
