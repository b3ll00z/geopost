package com.example.andrea.geopost.fragments;


import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.model.GeoPostUser;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.GeoUserAdapter;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FriendsFragment extends Fragment {

    private ListView friendsList;
    private GeoUserAdapter adapter;
    private List<GeoPostUser> friends;
    private double myLastLatitude;
    private double myLastLongitude;
    private LinearLayout layoutNoFriends;
    private String session_id;

    //Required empty public constructor
    public FriendsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //recupero la view del fragment
        View view = inflater.inflate(R.layout.fragment_friends_list, container, false);
        friendsList = view.findViewById(R.id.friendsList);
        layoutNoFriends = view.findViewById(R.id.layoutNoFriends);

        //prelevo il session ID per richimare VolleyRequestHandler
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        session_id = sharedPref.getString("SESSION_ID", null);

        //recupero mia ultima posizione, servirà per ordinare la lista di amici
        fetchMyLastPosition();

        //prelevo la lista degli amici
        fetchFriends();



        return view;
    }

    private void fetchMyLastPosition() {
        VolleyRequestHandler.getInstance(getActivity().getApplicationContext(), session_id)
                .profile(new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {

                        try {
                            myLastLatitude  = response.getDouble("lat");
                            myLastLongitude = response.getDouble("lon");

                            Log.d("FriendsFragment", "Last latitude: "  + myLastLatitude);
                            Log.d("FriendsFragment", "Last longitude: " + myLastLongitude);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getActivity().getApplicationContext(), "Errore rilevamento posizione", Toast.LENGTH_SHORT).show();
                        Log.d("FriendsFragment", "Errore rilevamento posizione");
                    }

                    @Override
                    public void onStringRequestSuccess(String response) { return; }
                });
    }

    private void fetchFriends() {
        //eseguo la richiesta al web service implementato in onSuccess e onError le cose da fare una volta ricevuta la risposta
        VolleyRequestHandler
                .getInstance(getActivity().getApplicationContext(), session_id)
                .fetchFriendsRequest(new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        JSONArray users = null;
                        Gson gson = new Gson();
                        try {
                            users = response.getJSONArray("followed");
                            friends = new ArrayList<GeoPostUser>();
                            for (int friendIndex = 0; friendIndex < users.length(); friendIndex++)
                                friends.add(gson.fromJson(users.get(friendIndex).toString(), GeoPostUser.class));

                            //ordino gli amici in base alla distanza
                            sortFriendsList();

                            //Costruisco l'adapter per gli amici
                            adapter = new GeoUserAdapter(getContext(), R.layout.custom_friend_list_row, friends);
                            friendsList.setAdapter(adapter);

                            if(friends.isEmpty())
                                layoutNoFriends.setVisibility(View.VISIBLE);

                            Log.d("FriendsFragment", "Amici ricevuti: " + users.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getActivity().getApplicationContext(), "Errore caricamento amici", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        return;
                    }
                });
    }

    private void sortFriendsList() {

        for (GeoPostUser friend : friends)
            setDistanceInkm(friend);

        Collections.sort(friends);
    }

    private void setDistanceInkm(GeoPostUser u){
        Location myLocation = new Location("mylocation");
        myLocation.setLatitude(this.myLastLatitude);
        myLocation.setLongitude(this.myLastLongitude);

        Location otherLocation = new Location("otherLocation");
        otherLocation.setLatitude(u.getLat());
        otherLocation.setLongitude(u.getLon());

        u.setDistanceInKmFromMe(myLocation.distanceTo(otherLocation));
    }

//    private void setDistanceInKm(GeoPostUser u) {
//
//        double theta = u.getLon() - myLastLongitude;
//        double dist = Math.sin(deg2rad(u.getLat())) * Math.sin(deg2rad(myLastLatitude)) +
//                        Math.cos(deg2rad(u.getLat())) * Math.cos(deg2rad(myLastLatitude)) *
//                                Math.cos(deg2rad(theta));
//
//        dist = Math.acos(dist);
//        dist = rad2deg(dist);
//        dist = dist * 60 * 1.1515;
//        u.setDistanceInKmFromMe(dist * 1.609344);
//    }
//
//    /* Conversione gradi a radianti */
//    private double deg2rad(double deg) {
//        return (deg * Math.PI / 180.0);
//    }
//
//    /* Conversione da radianti a decimali */
//    private double rad2deg(double rad) {
//        return (rad * 180.0 / Math.PI);
//    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMyLastPosition();
        fetchFriends();
    }
}
