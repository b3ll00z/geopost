package com.example.andrea.geopost.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.model.GeoPostUser;
import com.example.andrea.geopost.utilities.DataFetchCallBack;
import com.example.andrea.geopost.utilities.VolleyRequestHandler;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

public class FriendsMapFragment extends Fragment implements OnMapReadyCallback {

    private List<GeoPostUser> friendsToMarkOnMap;
    private Type listType = new TypeToken<List<GeoPostUser>>() {
    }.getType();
    private GoogleMap googleMap = null;

    public FriendsMapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_friends_map, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.friends_map);
        mapFragment.getMapAsync(this);

        fetchFriends();

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        Log.d("FriendsMapFragment", "Mappa caricata");
    }

    private void setMap() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (GeoPostUser friend : friendsToMarkOnMap) {

            if (friend.isSomewhere()) {
                MarkerOptions marker = new MarkerOptions()
                        .position(new LatLng(friend.getLat(), friend.getLon()))
                        .title(friend.getUsername())
                        .snippet(friend.getMsg());

                googleMap.addMarker(marker);
                builder.include(marker.getPosition());

                Log.d("FriendsMapFragment", friend.getUsername() + " caricato");
            }
        }

        int padding = 40;

        LatLngBounds mapBounds = null;
        for(GeoPostUser friend : friendsToMarkOnMap){
            if(mapBounds == null){
                LatLng point = new LatLng(friend.getLat(), friend.getLon());
                mapBounds = new LatLngBounds(point, point);
            } else {
                mapBounds = mapBounds.including(new LatLng(friend.getLat(), friend.getLon()));
            }
        }
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(mapBounds, padding);
        googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);
    }

    private void fetchFriends() {
        //prelevo il session ID per richimare VolleyRequestHandler
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String session_id = sharedPref.getString("SESSION_ID", null);

        VolleyRequestHandler
                .getInstance(getActivity().getApplicationContext(), session_id)
                .fetchFriendsRequest(new DataFetchCallBack() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        JSONArray users = null;
                        try {
                            users = response.getJSONArray("followed");
                            friendsToMarkOnMap = new Gson().fromJson(String.valueOf(users), listType);
                            Log.d("FriendsOnMapFragment", users.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (friendsToMarkOnMap.size() != 0)
                            setMap();
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getActivity().getApplicationContext(), "Errore caricamento amici", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onStringRequestSuccess(String response) {
                        return;
                    }
                });
    }
}
