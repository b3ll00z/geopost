package com.example.andrea.geopost.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.text.DecimalFormat;

public class GeoPostUser implements Parcelable, Comparable<GeoPostUser> {

    private String username;
    private String msg;
    private double lat;
    private double lon;
    private double distanceInKmFromMe;

    public GeoPostUser(){}

    public GeoPostUser(String username, String msg, double lat, double lon) {
        this.username  = username;
        this.msg = msg;
        this.lat = lat;
        this.lon = lon;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getDistanceInKmFromMe() { return distanceInKmFromMe; }

    public void setDistanceInKmFromMe(double distanceInKmFromMe) { this.distanceInKmFromMe = distanceInKmFromMe; }

    public String getFormattedDistance() {

        DecimalFormat df = new DecimalFormat("#.###");
        return df.format(distanceInKmFromMe) + " Km";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(msg);
        parcel.writeDouble(lat);
        parcel.writeDouble(lon);
    }

    public static final Parcelable.Creator<GeoPostUser> CREATOR
            = new Parcelable.Creator<GeoPostUser>() {
        public GeoPostUser createFromParcel(Parcel in) {
            return new GeoPostUser(in);
        }

        public GeoPostUser[] newArray(int size) {
            return new GeoPostUser[size];
        }
    };

    private GeoPostUser(Parcel in) {
        username = in.readString();
        msg = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
    }

    public boolean isSomewhere() {
        return Double.valueOf(lat) != 0 && Double.valueOf(lon) != 0;
    }

    @Override
    public int compareTo(@NonNull GeoPostUser o) {
        return Double.compare(this.getDistanceInKmFromMe(), o.getDistanceInKmFromMe());
    }
}
