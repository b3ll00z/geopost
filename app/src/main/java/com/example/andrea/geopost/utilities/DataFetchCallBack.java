package com.example.andrea.geopost.utilities;

import org.json.JSONObject;

/**
 * Created by Davide on 19/01/2018.
 */

/**
 * Interfaccia con lo scopo di formare una callback per il meccanismo delle richieste al web service. (SCOPO SINCRONIZZAZIONE)
 * Separo la logica che si occupa di effettuare le richieste, dal meccanismo di costruzione della classe di Model e
 * di rappresentazione della stessa.
 * FUNZIONAMENTO: ogni metodo di VolleyRequestHandler deve prendere come parametro questa interfaccia, e richiamerà i metodi
 * onSuccess ed onError rispettivamente nei listener onResponse ed onError.
 * Quando ciò avviene, la classe che chiama il metodo di servizio verrà "notificata" di procedere con le sue operazioni
 * (es. aggiornare un elemento di UI con i dati derivanti dalla risposta del WebService)
 */
public interface DataFetchCallBack {
    void onSuccess(JSONObject response);
    void onError(String error);
    void onStringRequestSuccess(String response);
}
