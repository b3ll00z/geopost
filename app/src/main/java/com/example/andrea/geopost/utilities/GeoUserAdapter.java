package com.example.andrea.geopost.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.andrea.geopost.R;
import com.example.andrea.geopost.model.GeoPostUser;

import java.util.List;

/**
 * Created by Davide on 19/01/2018.
 */

public class GeoUserAdapter extends ArrayAdapter<GeoPostUser>{

    public GeoUserAdapter(Context context, int resourceID   ) {
        super(context, resourceID);
    }

    public GeoUserAdapter(Context context, int resource, List<GeoPostUser> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.custom_friend_list_row, null);
        }
        //prelevo l'oggetto in posizione pos
        GeoPostUser friend = getItem(position);
        if (friend != null) {
            TextView friendName = (TextView) v.findViewById(R.id.friendNameTextView);
            TextView friendStatus = (TextView) v.findViewById(R.id.friendStatusTextView);
            TextView friendDistance = (TextView) v.findViewById(R.id.friendDistanceTextView);
            friendName.setText(friend.getUsername());
            friendStatus.setText(friend.getMsg());
            friendDistance.setText(friend.getFormattedDistance());
        }
        return v;
    }
}

