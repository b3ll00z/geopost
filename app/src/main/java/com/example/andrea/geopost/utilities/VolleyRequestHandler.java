package com.example.andrea.geopost.utilities;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Davide on 19/01/2018.
 */

public class VolleyRequestHandler {
    private static VolleyRequestHandler instance = null;
    private static RequestQueue mRequestQueue = null;
    private static Context mContext = null;
    private static String sessionID = null;

    //costanti per le richieste al web service (NECESSARIO CONCATENARE SESSION ID QUANDO OCCORRE)
    public static final String URL_LOGIN = "https://ewserver.di.unimi.it/mobicomp/geopost/login";
    public static final String URL_FETCH_FRIENDS = "https://ewserver.di.unimi.it/mobicomp/geopost/followed?session_id=";
    public static final String URL_STATUS_UPDATE = "https://ewserver.di.unimi.it/mobicomp/geopost/status_update?session_id=";
    public static final String URL_LOGOUT = "https://ewserver.di.unimi.it/mobicomp/geopost/logout?session_id=";
    public static final String URL_USERS = "https://ewserver.di.unimi.it/mobicomp/geopost/users?session_id=";
    public static final String URL_FOLLOW = "https://ewserver.di.unimi.it/mobicomp/geopost/follow?session_id="; //concatenare anche username dell'amico
    public static final String URL_PROFILE = "https://ewserver.di.unimi.it/mobicomp/geopost/profile?session_id=";

    /**
     * Costruttore dell'istanza quando entrambi i parametri vengono specificati
     *
     * @param context    contesto dell'activity
     * @param session_id session ID dell'utente
     */
    private VolleyRequestHandler(Context context, String session_id) {
        setupContextData(context);
        sessionID = session_id;
    }

    /**
     * Costruttore dell'istanza quando solo il contesto viene fornito, senza session ID (Caso di login)
     *
     * @param context contesto dell'activity
     */
    private VolleyRequestHandler(Context context) {
        setupContextData(context);
    }

    //Avvalora tutti i campi privati che dipendono da context
    private void setupContextData(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    /**
     * Ritorna l'istanza della classe
     *
     * @param context    contesto (preferibile quello dell'applicazione)
     * @param session_id session_id dell'utente
     * @return istanza di VolleyRequestHandler
     */
    public static synchronized VolleyRequestHandler getInstance(Context context, String session_id) {
        if (instance == null) {
            if (session_id != null)
                instance = new VolleyRequestHandler(context, session_id);
            else
                //se session_id == null significa che il metodo è chiamato dall'activity per il login (che non possiede session_id)
                instance = new VolleyRequestHandler(context);
        } else if (sessionID == null && session_id != null) {
            sessionID = session_id;
        }
        return instance;
    }

    /**
     * Effettua la richiesta per gli amici aggiunti
     *
     * @param callBack interfaccia per passare al chiamante la response
     */
    public void fetchFriendsRequest(final DataFetchCallBack callBack) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_FETCH_FRIENDS + sessionID, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callBack.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error.getMessage());
            }
        });
        mRequestQueue.add(request);
    }

    /**
     * Effettua una richiesta di login al web service
     *
     * @param username username con il quale si desidera accedere
     * @param password password digitata
     * @param callBack interfaccia per sincronizzare il chiamante
     */
    public void login(String username, String password, final DataFetchCallBack callBack) {
        StringRequest request = new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callBack.onStringRequestSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("username", username);
                map.put("password", password);
                return map;
            }
        };
        mRequestQueue.add(request);
    }

    /**
     * Richiede al web service il logout
     *
     * @param callBack interfaccia per sincronizzare il chiamante
     */
    public void logout(final DataFetchCallBack callBack) {
        StringRequest request = new StringRequest(Request.Method.POST, URL_LOGOUT + sessionID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionID = null;
                callBack.onStringRequestSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error.getMessage());
            }
        });
        mRequestQueue.add(request);
    }

    /**
     * Notifica il web service di un aggiornamento di stato da parte dell'utente
     *
     * @param parameters parametri necessari al web service per aggiornare i dati
     * @param callBack   interfaccia per sincronizzare il chiamante
     */
    public void updateStatus(String parameters, final DataFetchCallBack callBack) {
        Request updateStatusRequest = new StringRequest(Request.Method.GET, URL_STATUS_UPDATE + sessionID + parameters,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        callBack.onStringRequestSuccess(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onError(error.getMessage());
                    }
                });
        mRequestQueue.add(updateStatusRequest);
    }

    /**
     * Ritorna la lista di utenti che iniziano con startwith
     *
     * @param startWith caratteri alfanumerici per la ricerca degli utenti
     * @param callBack  interfaccia per sincronizzare il chiamante
     */
    public void searchUsers(String startWith, final DataFetchCallBack callBack) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET, URL_USERS + sessionID + startWith, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callBack.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error.getMessage());
            }
        });
        mRequestQueue.add(request);
    }

    /**
     * Aggiunte un utente alla lista di followers
     *
     * @param parametersForURL
     * @param callBack
     */
    public void addUser(String parametersForURL, final DataFetchCallBack callBack) {
        Request updateStatusRequest = new StringRequest(Request.Method.GET, URL_FOLLOW + sessionID + parametersForURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callBack.onStringRequestSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onError(new String(error.networkResponse.data));
                    }
                });
        mRequestQueue.add(updateStatusRequest);
    }

    /**
     * Acquisisce informazioni sul profilo utente, quali username, ultimo status e ultima posizione
     *
     * @param callBack
     */
    public void profile(final DataFetchCallBack callBack) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET, URL_PROFILE + sessionID, null,
           new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    callBack.onSuccess(response);
                }
        }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callBack.onError(error.getMessage());
                }
        });
        mRequestQueue.add(request);
    }
}
